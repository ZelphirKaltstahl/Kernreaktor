;; #lang racket

(define-module (bitboard)
  #:export (create-bitboard
            get-coord
            index->board-coords
            index->coords
            get-coords-of-trues
            binary-bit-vector-operation
            bb-binary-operation
            bb-unary-operation
            bb-fold
            bb-and
            bb-or
            bb-not
            bb-any
            bb-xor
            make-bitboard
            bitboard-bits
            bitboard-height
            bitboard-width
            bitboard-kind))

;; SRFI 9: record types
(use-modules (srfi srfi-9)
             (bit-vector-utils))

(define-record-type <bitboard>
  ;; define constructor
  (make-bitboard bits height width kind)
  ;; define predicate
  bitboard?
  ;; define accessors
  (bits bitboard-bits)
  (height bitboard-height)
  (width bitboard-width)
  (kind bitboard-kind))


(define* (display-bitboard board #:optional (port (current-output-port)))
  (display
   (simple-format port
                  "Bitbard: <bits: ~a, height: ~a, width: ~a, kind: ~s>"
                  (bitboard-bits board)
                  (bitboard-height board)
                  (bitboard-width board)
                  (bitboard-kind board))))


(define* (create-bitboard #:optional (height 6) (width 6)
                          #:key (kind 'kernreaktor) (initial #f))
  (let ([num-bits (* height width)])
    (make-bitboard (make-bitvector num-bits initial)
                   height
                   width
                   kind)))


(define (get-coord board row-ind col-ind)
  #;(display (simple-format #f "trying to get coord: row-ind:~a col-ind:~a\n"
                          row-ind
                          col-ind))
  (cond [(> row-ind (1- (bitboard-height board)))
         (let ([error-message
                (simple-format #f "row-ind: ~a out of bounds of board: ~a" row-ind board)])
           (throw 'index-error error-message board row-ind))]
        [(> col-ind (1- (bitboard-width board)))
         (let ([error-message
                (simple-format #f "col-ind: ~a out of bounds of board: ~a" col-ind board)])
           (throw 'index-error error-message board col-ind))]
        [else
         (let* ([height (bitboard-height board)]
                [width (bitboard-width board)]
                [index (+ (* row-ind width) col-ind)])
           (bitvector-ref (bitboard-bits board) index))]))


(define (index->board-coords index board)
  #;(display (simple-format #f "index->board-coords ~a ~a\n"
                          index board))
  (let ([row-count (bitboard-height board)]
        [col-count (bitboard-width board)])
    (index->coords index row-count col-count)))


(define (index->coords index row-count col-count)
  #;(display (simple-format #f "index->coords ~a ~a ~a\n"
                          index row-count col-count))
  (cond [(>= index (* row-count col-count))
           (throw 'index-error
                  (simple-format #f "expected index < ~a" (* row-count col-count))
                  index row-count col-count)]
        [(not (and (exact-integer? index)
                   (>= index 0)))
         (throw 'index-error
                "expected index to be exact positive integer or exact integer 0"
                index)]
        [(let* ([row-coord (floor (/ index col-count))]
                [col-coord (- index (* row-coord col-count))])
           (cons row-coord col-coord))]))


(define (get-coords-of-trues board)
  (define (iter bits row-count col-count index)
    #;(display (simple-format #f "iter ~a ~a ~a ~a\n"
                            bits row-count col-count index))
    (cond
     [(< index (bitvector-length bits))
      (let ([bit (bitvector-ref bits index)])
        (cond
         [bit (cons (index->coords index row-count col-count)
                    (iter bits
                          row-count
                          col-count
                          (+ index 1)))]
         [else (iter bits
                     row-count
                     col-count
                     (+ index 1))]))]
     [else '()]))
  #;(display (simple-format #f "get-coords-of-trues ~a\n" board))
  (iter (bitboard-bits board)
        (bitboard-height board)
        (bitboard-width board)
        0))


(define (binary-bit-vector-operation bv1 bv2 proc)
  (define (iter bit-list1 bit-list2)
    (cond [(or (null? bit-list1) (null? bit-list2)) '()]
          [else (cons (proc (car bit-list1)
                            (car bit-list2))
                      (iter (cdr bit-list1)
                            (cdr bit-list2)))]))
  #;(display (simple-format #f "binary-bit-vector-operation: ~a ~a ~a\n" bv1 bv2 proc))
  (list->bitvector
   (iter (bitvector->list bv1)
         (bitvector->list bv2))))


(define (bb-binary-operation board1 board2 proc)
  #;(display (simple-format #f "bb-binary-operation: b1: ~a,\nb2: ~a, proc: ~a\n" board1 board2 proc))
  (let ([bits-b1 (bitboard-bits board1)]
        [bits-b2 (bitboard-bits board2)])
    #;(display (simple-format #f "in let in bb-binary-operation\n"))
    ;; until here it runs fine
    (make-bitboard (binary-bit-vector-operation bits-b1 bits-b2 proc)
                   (min (bitboard-height board1)
                        (bitboard-height board2))
                   (min (bitboard-width board1)
                        (bitboard-width board2))
                   (if (equal? (bitboard-kind board1)
                               (bitboard-kind board2))
                       (bitboard-kind board1)
                       'unspecified))))


(define (bb-unary-operation board proc)
  (let ([bits (bitboard-bits board)])
    (make-bitboard (list->bitvector
                    (map proc
                         (bitvector->list (bitboard-bits board))))
                   (bitboard-height board)
                   (bitboard-width board)
                   (bitboard-kind board))))


(define (bb-fold board proc initial)
  (define (iter accumulated ind bits proc)
    (cond [(> ind (- (bitvector-length bits) 1))
           accumulated]
          [else
           (let ([current-bit (bitvector-ref bits ind)])
             (iter (proc accumulated current-bit)
                   (+ ind 1)
                   bits
                   proc))]))
  (iter initial
        0
        (bitboard-bits board)
        proc))



(define (bb-and board1 board2)
  (bb-binary-operation board1
                       board2
                       (λ (bit1 bit2)
                         (and bit1 bit2))))

(define (bb-or board1 board2)
  #;(display (simple-format #f "b1: ~a,\nb2: ~a\n" board1 board2))
  (bb-binary-operation board1
                       board2
                       (λ (bit1 bit2)
                         (or bit1 bit2))))

(define (bb-not board)
  (bb-unary-operation board
                      (λ (a-bit)
                        (not a-bit))))

(define (bb-any board)
  (bb-fold board
           (λ (acc a-bit)
             (or acc a-bit))
           #f))

(define (bb-xor board1 board2)
  (bb-binary-operation board1
                       board2
                       (λ (bit1 bit2)
                         (or (and (not bit1) bit2)
                             (and bit1 (not bit2))))))
