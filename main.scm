(add-to-load-path
 (string-append "/home/xiaolong"
                "/development/Guile"
                "/examples-and-convenience-procedures/network-programming"))
(use-modules ((tcp-server) #:prefix server:)
             ((tcp-client) #:prefix client:)
             (ice-9 textual-ports)
             (json))


(define (json-echo-message-handler client-connection scm-native)
  (let ([in-out-sock (car client-connection)])
    (display (simple-format #f "RECEIVED JSON: ~s, which is: ~s\n"
                            scm-native
                            (scm->json-string scm-native)))
    (scm->json scm-native in-out-sock)
    (force-output in-out-sock)
    (display (simple-format #f "SENT JSON: ~s, which is: ~s\n"
                            scm-native
                            (scm->json-string scm-native)))))

(define json-echo-protocol
  (server:make-server-protocol
   #:port-reader json->scm
   #:message-handler json-echo-message-handler
   #:eof-handler server:shutdown-client-connection))


(define (client-json-display-message-handler in-out-sock scm-native)
  (display (simple-format #f "RECEIVED JSON: ~s, which is: ~s\n"
                          scm-native
                          (scm->json-string scm-native))))

(define client-json-display-protocol
  (client:make-client-protocol
   #:port-reader json->scm
   #:message-handler client-json-display-message-handler
   #:eof-handler close))

;; =========
;; TEST AREA
;; =========

;; LINE ECHO SERVER
;; (define server-sock
;;   (server:run-server 12345
;;                      #:protocol server:echo-protocol))
;; (define client-sock
;;   (client:run-client 12345
;;                      #:protocol client:display-message-protocol))

;; JSON ECHO SERVER
(define server-sock
  (server:run-server 12345
                     #:protocol json-echo-protocol))

(define client-sock
  (client:run-client 12345
                     #:protocol client-json-display-protocol))

(scm->json '((a . 1)) client-sock)

;; https://github.com/aconchillo/guile-json
#;(put-string client-sock
            (string-append (scm->json-string '((a . 3)))
                           "\n"))
