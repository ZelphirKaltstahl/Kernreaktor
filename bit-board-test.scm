(add-to-load-path
 (string-append "/home/xiaolong"
                "/development/Guile"
                "/Kernreaktor"))
(use-modules (srfi srfi-64)
             (bit-vector-utils)
             (bitboard))

(test-begin "bitboard")

(test-group
 "create-bitboard-test"
 (let ([test-board (create-bitboard 10 10 #:kind 'unspecified #:initial #t)])
   (test-equal (bitboard-bits test-board) (make-bitvector (* 10 10) #t))
   (test-eqv (bitboard-kind test-board) 'unspecified)
   (test-eqv (bitboard-height test-board) 10)
   (test-eqv (bitboard-width test-board) 10)))

(test-group
 "get-coord-test"
 (let ([test-board (make-bitboard
                    (string->bit-vector
                     (string-append "000000001"
                                    "000000010"
                                    "000000100"
                                    "000001000"
                                    "000010000"
                                    "000100000"
                                    "001000000"
                                    "010000000"
                                    "100000000"))
                    9
                    9
                    'shogi)])
   (do ([row-ind 0 (+ row-ind 1)])
       ([>= row-ind 9])
     (do ([col-ind 0 (+ col-ind 1)])
         ([>= col-ind 9])
       #;(display (simple-format #f "get-coord-test with row-ind: ~a col-ind: ~a\n"
                               row-ind col-ind))
       (cond
        [(= (+ row-ind col-ind) 8)
         (test-eqv (get-coord test-board row-ind col-ind) #t)]
        [else
         (test-eqv (get-coord test-board row-ind col-ind) #f)])))
   #;(display (simple-format #f "after loops\n"))))


(test-group
 "bb-or-test"
 #;(display (simple-format #f "before let\n"))
 (let* ([or-board
         (bb-or (make-bitboard
                 (string->bit-vector
                  (string-append "000000001"
                                 "000000010"
                                 "000000100"
                                 "000001000"
                                 "000010000"
                                 "000100000"
                                 "001000000"
                                 "010000000"
                                 "100000000"))
                 9
                 9
                 'shogi)
                (make-bitboard
                 (string->bit-vector
                  (string-append "100000000"
                                 "010000000"
                                 "001000000"
                                 "000100000"
                                 "000010000"
                                 "000001000"
                                 "000000100"
                                 "000000010"
                                 "000000001"))
                 9
                 9
                 'shogi))])
   #;(display (simple-format #f "before loops\n"))
   (do ([row-ind 0 (+ row-ind 1)])
       ([>= row-ind 9])
     (do ([col-ind 0 (+ col-ind 1)])
         ([>= col-ind 9])
       (cond
        [(or (= (+ row-ind col-ind) 8) (= row-ind col-ind))
         (test-eqv (get-coord or-board row-ind col-ind) #t)]
        [else
         (test-eqv (get-coord or-board row-ind col-ind) #f)])))
   #;(display (simple-format #f "after loops\n"))))


;; todo: write tests for: index->board-coords
;; todo: make caught exceptions more specific
(test-group
 "index->coords-test"
 #;(test-error [[test-name] error-type] test-expr)
 (test-error "index out of bounds" #t
             (index->coords 81 9 9))
 (test-error "index out of bounds" #t
             (index->coords 81 10 8))
 (test-error "index out of bounds" #t
             (index->coords 80 8 10))
 (test-error "index out of bounds" #t
             (index->coords 6 3 2))
 (test-error "index out of bounds" #t
             (index->coords 7 3 2))
 (test-error "index out of bounds" #t
             (index->coords 1 1 1))
 (test-error "negative number of rows" #t
             (index->coords 81 -1 9))
 (test-error "negative number of cols" #t
             (index->coords 81 9 -1))
 (test-error "negative number of rows and cols" #t
             (index->coords 81 -4 -4))
 (test-error "negative index" #t
             (index->coords -70 9 9))

 (test-equal (index->coords 0 1 1) (cons 0 0))
 (test-equal (index->coords 40 9 9) (cons 4 4))
 (test-equal (index->coords 999 10 100) (cons 9 99))
 (test-equal (index->coords 14 5 4) (cons 3 2))
 (test-equal (index->coords 543 55 10) (cons 54 3))
 (test-equal (index->coords 74 10 10) (cons 7 4)))


(test-group
 "get-coords-of-trues-test"
 (let ([test-board (make-bitboard
                    (string->bit-vector
                     (string-append "000000001"
                                    "000000010"
                                    "000000100"
                                    "000001000"
                                    "000010000"
                                    "000100000"
                                    "001000000"
                                    "010000000"
                                    "100000000"))
                    9
                    9
                    'shogi)])
   (test-equal (get-coords-of-trues test-board)
     '((0 . 8)
       (1 . 7)
       (2 . 6)
       (3 . 5)
       (4 . 4)
       (5 . 3)
       (6 . 2)
       (7 . 1)
       (8 . 0))))
 (let ([test-board (make-bitboard
                    (string->bit-vector
                     (string-append "111111111"
                                    "100000001"
                                    "101111101"
                                    "101000101"
                                    "101010101"
                                    "101000101"
                                    "101111101"
                                    "100000001"
                                    "111111111"))
                    9
                    9
                    'shogi)])
   (test-equal (get-coords-of-trues test-board)
     (append '((0 . 0) (0 . 1) (0 . 2) (0 . 3) (0 . 4) (0 . 5) (0 . 6) (0 . 7) (0 . 8))
             '((1 . 0) (1 . 8))
             '((2 . 0) (2 . 2) (2 . 3) (2 . 4) (2 . 5) (2 . 6) (2 . 8))
             '((3 . 0) (3 . 2) (3 . 6) (3 . 8))
             '((4 . 0) (4 . 2) (4 . 4) (4 . 6) (4 . 8))
             '((5 . 0) (5 . 2) (5 . 6) (5 . 8))
             '((6 . 0) (6 . 2) (6 . 3) (6 . 4) (6 . 5) (6 . 6) (6 . 8))
             '((7 . 0) (7 . 8))
             '((8 . 0) (8 . 1) (8 . 2) (8 . 3) (8 . 4) (8 . 5) (8 . 6) (8 . 7) (8 . 8))))))
(test-end "bitboard")
