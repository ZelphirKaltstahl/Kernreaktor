(define-module (bit-vector-utils)
  #:export (string->bit-vector))

(define (string->bit-vector str)
  (list->bitvector
   (map (λ (char)
          (cond
           [(char=? char #\0) #f]
           [(char=? char #\1) #t]
           [else
            (let ([error-message "trying to convert the following character to boolean:"])
              (throw 'value-error error-message char))]))
        (string->list str))))
