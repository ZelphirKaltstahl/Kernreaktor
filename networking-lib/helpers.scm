#; (use-modules (rnrs bytevectors))

;; see also: https://www.gnu.org/software/guile/manual/html_node/Modules-and-the-File-System.html#Modules-and-the-File-System
(define-module (networking-lib helpers)
  #:export (display-byte-vector
            byte-vector->utf8-message
            send-message-on-socket
            make-protocol))

(use-modules (rnrs bytevectors)
             (ice-9 textual-ports))

(define (byte-vector->utf8-message bv len)
  (when (= len 0) (display "length was zero"))
  (let ([minimal-bv (make-bytevector len)])
        ;; bytevector-copy! source source-start target target-start len
        (bytevector-copy! bv 0 minimal-bv 0 len)
        (utf8->string minimal-bv)))

(define (display-byte-vector vec len)
  (display (simple-format #f
                          "I got the following message: ~s\n"
                          (byte-vector->utf8-message vec len))))

(define (send-message-on-socket sock message)
  (display (simple-format #f "sending message: ~s\n" message))
  (display (simple-format #f "representation: ~s\n" (string->utf8 message)))
  (send sock (string->utf8 message)))
